import logging

from logging.handlers import TimedRotatingFileHandler

from ..config import LOG_DIR

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.INFO)

debug_file_handler = TimedRotatingFileHandler(
    filename=LOG_DIR / 'debug.log',
    when='midnight'
)
debug_file_handler.setLevel(logging.DEBUG)
debug_file_handler.setFormatter(formatter)

info_file_handler = TimedRotatingFileHandler(
    filename=LOG_DIR / 'info.log',
    when='midnight'
)
info_file_handler.setLevel(logging.DEBUG)
info_file_handler.setFormatter(formatter)

warn_file_handler = TimedRotatingFileHandler(
    filename=LOG_DIR / 'warn.log',
    when='midnight'
)
warn_file_handler.setLevel(logging.DEBUG)
warn_file_handler.setFormatter(formatter)

error_file_handler = TimedRotatingFileHandler(
    filename=LOG_DIR / 'error.log',
    when='midnight'
)
error_file_handler.setLevel(logging.DEBUG)
error_file_handler.setFormatter(formatter)
