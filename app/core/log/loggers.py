import logging

from .handlers import (debug_file_handler,
                       info_file_handler,
                       warn_file_handler,
                       error_file_handler)


def setup_logger_with_handlers(logger_name: str, level: int = logging.DEBUG):
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)

    logger.addHandler(debug_file_handler)
    logger.addHandler(info_file_handler)
    logger.addHandler(warn_file_handler)
    logger.addHandler(error_file_handler)


def setup_loggers():
    setup_logger_with_handlers('aiogram', logging.DEBUG)
    setup_logger_with_handlers('db', logging.DEBUG)
    setup_logger_with_handlers('api', logging.DEBUG)
    setup_logger_with_handlers('bot', logging.DEBUG)
    setup_logger_with_handlers('sqlalchemy.engine', logging.INFO)
    setup_logger_with_handlers('aiohttp.access', logging.DEBUG)
