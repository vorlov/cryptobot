import asyncio
import logging

import celery

from datetime import datetime, timedelta
from pytimeparse.timeparse import timeparse
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from asgiref.sync import async_to_sync

from app.bot import bot
from app.api.utils import get_price_from_pair
from app.core.celeryconfig import app
from app.core.config import settings
from app.db.repositories.user import ClientRepository
from app.db.repositories.tracking import PairTrackingRepository

log = logging.getLogger(__name__)


class SqlAlchemySessionContext(celery.Task):
    def before_start(self, task_id, args, kwargs):
        self.__sql_alchemy_engine = create_async_engine(settings.async_database_url)

        self._async_session = sessionmaker(
            self.__sql_alchemy_engine,
            class_=AsyncSession,
            expire_on_commit=False
        )


async def async_check_tracking_intervals(session: AsyncSession):
    client_repository = ClientRepository(session)
    pair_tracking_repository = PairTrackingRepository(session)

    trackings = await pair_tracking_repository.get_active_trackings()
    notified_count = 0

    if not trackings:
        return

    for tracking in trackings:
        # To prevent rate limit
        if notified_count == 25:
            await asyncio.sleep(1)

        if datetime.now() > tracking.last_notified + timedelta(seconds=timeparse(tracking.interval)):
            client = await client_repository.get_by_id(entry_id=tracking.client_id)
            new_price = await get_price_from_pair(tracking.currency_to_get, tracking.currency_to_spend)

            if new_price == tracking.last_price:
                continue

            await bot.send_message(
                chat_id=client.bot.chat_id,
                text=f'Notify: <b>{tracking.mnemonic_pair.replace("_", "→")}</b> is just changed:\n'
                     f'Old price: <b>{tracking.last_price}</b>\n'
                     f'New price: <b>{new_price} {"📈" if new_price > tracking.last_price else "📉"}</b>'
            )

            log.info(f'Client<id:{client.id}> has been notified about {tracking.mnemonic_pair.replace("_", "→")}')
            await pair_tracking_repository.update_by_id(entry_id=tracking.id, last_price=new_price,
                                                        last_notified=datetime.now())

            notified_count += 1

    await session.close()


@app.task(
    bind=True,
    base=SqlAlchemySessionContext
)
def check_tracking_intervals(self):
    session = self._async_session()

    async_to_sync(async_check_tracking_intervals)(session)
