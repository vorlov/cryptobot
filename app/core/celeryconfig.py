from celery import Celery
from app.core.config import settings

app = Celery('tasks', broker=settings.celery.broker,
             include=['app.core.tasks'])

app.conf.beat_schedule = {
    'check-tracking-intervals': {
        'task': 'app.core.tasks.check_tracking_intervals',
        'schedule': 5
    }
}
