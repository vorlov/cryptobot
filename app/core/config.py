import pathlib

from typing import Literal, Optional

from pydantic import BaseSettings, BaseModel, validator, RedisDsn, PostgresDsn
from yaml import safe_load

BASE_DIR = pathlib.Path(__file__).resolve(strict=True).parent.parent
ALEMBIC_BASE_DIR = BASE_DIR.parent
LOG_DIR = BASE_DIR.parent / 'logs'
LOG_DIR.mkdir(exist_ok=True)


class BotStorageSettings(BaseModel):
    mode: Literal['memory', 'redis'] = 'memory'
    redis_dsn: Optional[RedisDsn]

    @validator('redis_dsn')
    def validate_redis(cls, v, values):
        if v is None:
            if values['mode'] == 'redis':
                raise ValueError('redis_dsn must be filled if storage mode is redis')
        return v


class WebhookSettings(BaseModel):
    class ServerSettings(BaseModel):
        host: Optional[str] = 'localhost'
        port: Optional[str] = 80

        ssl_keyfile: Optional[str]
        ssl_certfile: Optional[str]

    path: str
    host: str
    server: Optional[ServerSettings] = ServerSettings()


class BotSettings(BaseModel):
    storage: Optional[BotStorageSettings] = BotStorageSettings()

    mode: Literal['webhook', 'polling'] = 'polling'
    token: str
    webhook: Optional[WebhookSettings]

    # @validator('mode', always=True)
    # def validate_webhook_mode(cls, v, values):
    #     print(values)
    #     if v == 'webhook':
    #         if values.get('webhook') is None:
    #             raise ValueError('webhook section must be filled if mode is webhook')
    #     return v


class CelerySettings(BaseModel):
    broker: RedisDsn
    backend: Optional[RedisDsn]

    @validator('backend')
    def validate_celery_backend(cls, v, values):
        if v is None:
            return values['broker']
        return v


class GeneralSettings(BaseSettings):
    bot: BotSettings
    celery: CelerySettings
    database_url: PostgresDsn

    @property
    def async_database_url(self) -> str:
        return self.database_url.replace('postgresql://', 'postgresql+asyncpg://') \
            if self.database_url \
            else self.database_url


def load_settings(mode: Literal['default', 'alembic'] = 'default'):
    if mode == 'alembic':
        try:
            settings_file = open(BASE_DIR.parent / 'settings.yaml', 'r')
        except FileNotFoundError:
            settings_file = open(BASE_DIR.parent / 'settings.yml', 'r')
    else:
        try:
            settings_file = open(BASE_DIR / 'settings.yaml', 'r')
        except FileNotFoundError:
            settings_file = open(BASE_DIR / 'settings.yml', 'r')

    raw_config = safe_load(settings_file.read())
    return GeneralSettings(**raw_config)


try:
    settings = load_settings()
except FileNotFoundError:
    settings = load_settings(mode='alembic')
