from typing import List, Literal, Optional

from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from aiogram.utils.callback_data import CallbackData


def main_menu_kb() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)

    markup.add(
        'Tracking 🎯',
        'Exchange 💱'
    )

    return markup


def back_to_main_menu_kb() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)

    markup.add('Main menu 🔙')

    return markup


def back_to_tracking_menu_kb() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)

    markup.add('Tracking 🔙')

    return markup


navigate_currencies_cb = CallbackData('currency', 'action', 'to_page')


def pages_inline_kb(
        page_elements: List[str],
        row_width: Optional[int] = 2,
        next_page_number: Optional[int] = None,
        previous_page_number: Optional[int] = None
) -> InlineKeyboardMarkup:
    imk = InlineKeyboardMarkup(row_width=row_width)

    imk.add(*[InlineKeyboardButton(text=cur, callback_data=cur) for cur in page_elements])

    navigation = []
    if previous_page_number is not None:
        navigation.append(
            InlineKeyboardButton(
                text='«',
                callback_data=navigate_currencies_cb.new(action='back', to_page=previous_page_number)
            ))

    if next_page_number is not None:
        navigation.append(
            InlineKeyboardButton(
                text='»',
                callback_data=navigate_currencies_cb.new(action='forward', to_page=next_page_number)
            ))

    if navigation:
        imk.add(*navigation)

    return imk


def tracking_menu_kb() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)

    markup.add('List tracking 🗂', 'New tracking 📍', 'Main menu 🔙')

    return markup


toggle_cb = CallbackData('tracking', 'action', '')


def pair_settings_kb(is_pair_active: bool) -> InlineKeyboardMarkup:
    markup = InlineKeyboardMarkup(row_width=2)

    toggle_button = InlineKeyboardButton(text='Activate', callback_data='toggle_on') if not is_pair_active \
        else InlineKeyboardButton(text='Dectivate', callback_data='toggle_off')

    markup.add(*[
        toggle_button,
        InlineKeyboardButton(text='Delete', callback_data='delete'),
        InlineKeyboardButton(text='Change interval', callback_data='change_interval'),
    ])

    markup.add(InlineKeyboardButton(text='« Back To Pairs List', callback_data='back_to_pairs'))

    return markup
