import asyncio

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text

from app.bot import dp
from app.bot.keyboards import pages_inline_kb, back_to_main_menu_kb, main_menu_kb
from app.bot.states import ExchangeFSM, MenuFSM
from app.db.repositories.action import ExchangeActionRepository
from app.db.repositories.user import ClientRepository
from app.models.schema.action import InExchangeActionSchema
from app.bot.handlers.utils import list_union, divide_list_by, flush_messages, perform_union_with_ordering
from app.api.coinpay import get_exchange_rate_list
from app.api.utils import get_exchange_currencies, get_price_from_pair


@dp.message_handler(Text(contains='Exchange'), state=MenuFSM.main_menu)
async def process_exchange_flow(message: types.Message, state: FSMContext,
                                client_repo: ClientRepository,
                                exchange_action_repo: ExchangeActionRepository):
    client = await client_repo.get_by_chat_id(message.chat.id)

    actions_to_get = await exchange_action_repo.get_latest_currency_to_get_by_user_id(client.id)
    actions_to_spend = await exchange_action_repo.get_latest_currency_to_spend_by_user_id(client.id)

    latest_get_currencies = list(map(lambda action: action.currency_to_get, actions_to_get))
    latest_spend_currencies = list(map(lambda action: action.currency_to_spend, actions_to_spend))

    currencies = get_exchange_currencies(await get_exchange_rate_list())
    currencies_to_get = list(currencies.keys())

    perform_union_with_ordering(latest_get_currencies, currencies_to_get)
    chunks = divide_list_by(to_divide=latest_get_currencies + currencies_to_get, n=5)

    await message.reply(
        text='In this section of the menu you can view the current exchange rate 💱. '
             'All exchange rates are taken from the <a href="https://coinpay.cr/">coinpay</a> database 💾.\n\n'
             '<i>Reminder: All your currency choices you have made are saved to bring them to the top of the list</i>',
        reply_markup=back_to_main_menu_kb()
    )
    await asyncio.sleep(0.5)

    mess = await message.bot.send_message(
        chat_id=message.chat.id,
        text='Choose the currency you want to <b>GET</b>:',
        reply_markup=pages_inline_kb(chunks[0],
                                     next_page_number=1 if len(chunks) > 1 else None)
    )

    async with state.proxy() as data:
        data['currencies'] = currencies
        data['page_chunks'] = chunks
        data['messages_to_flush'] = [mess.message_id]

        data['latest_get_currencies'] = latest_get_currencies
        data['latest_spend_currencies'] = latest_spend_currencies

    await ExchangeFSM.currency_to_get_input.set()


@dp.callback_query_handler(state=ExchangeFSM.currency_to_get_input)
async def process_currency_buy_input(
        query: types.CallbackQuery,
        state: FSMContext
):
    async with state.proxy() as data:
        spend_currencies = data['currencies'][query.data]
        latest_spend_currencies = data['latest_spend_currencies']

        perform_union_with_ordering(latest_spend_currencies, spend_currencies)
        chunks = divide_list_by(to_divide=latest_spend_currencies + spend_currencies, n=5)

        data['page_chunks'] = chunks
        data['to_get'] = query.data

    await query.message.edit_text(text=f'You chose <b>{query.data}</b>. '
                                       f'Choose the currency you want to <b>SPEND</b> to buy <b>{query.data}</b>:')
    await query.message.edit_reply_markup(
        reply_markup=pages_inline_kb(
            chunks[0],
            next_page_number=1
        )
    )

    await ExchangeFSM.currency_to_spend_input.set()


@dp.callback_query_handler(state=ExchangeFSM.currency_to_spend_input)
async def process_currency_buy_choose(
        query: types.CallbackQuery,
        exchange_action_repo: ExchangeActionRepository,
        client_repo: ClientRepository,
        state: FSMContext
):
    async with state.proxy() as data:
        to_spend = query.data
        to_get = data['to_get']
        messages_to_flush = data['messages_to_flush']

    price = await get_price_from_pair(
        currency_to_get=to_get,
        currency_to_spend=to_spend,
    )

    await query.message.delete()

    await query.bot.send_message(
        chat_id=query.message.chat.id,
        text=f'Your request is ready! ✅\n\n'
             f'Get: <b>1 {to_get}</b>\n'
             f'Spend: <b>{price} {to_spend}</b>',
        reply_markup=main_menu_kb()
    )

    await flush_messages(chat_id=query.message.chat.id, message_ids_to_flush=messages_to_flush)

    await exchange_action_repo.create(
        InExchangeActionSchema(
            currency_to_spend=to_spend,
            currency_to_get=to_get,
            client_id=(await client_repo.get_by_chat_id(query.message.chat.id)).id
        )
    )
    await MenuFSM.main_menu.set()
