import asyncio

from aiogram import types
from aiogram.dispatcher import FSMContext

from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.filters import OrFilter

from app.api.coinpay import get_exchange_rate_list
from app.api.utils import get_exchange_currencies, get_price_from_pair
from app.bot import dp
from app.bot.states import TrackingFSM, NewTrackingFSM, MenuFSM, ListTrackingFSM
from app.bot.keyboards import tracking_menu_kb, pages_inline_kb, back_to_tracking_menu_kb, pair_settings_kb
from app.bot.handlers.utils import divide_list_by, TIME_INTERVALS, flush_messages, clear_context, get_tracking_info_text

from app.models.schema.tracking import InPairTrackingSchema
from app.db.repositories.user import ClientRepository
from app.db.repositories.tracking import PairTrackingRepository


@dp.message_handler(
    OrFilter(
        Text(equals='Tracking 🎯'),
        Text(equals='Tracking 🔙')
    ), state=(MenuFSM.main_menu,
              NewTrackingFSM,
              ListTrackingFSM))
async def process_tracking_menu(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if data.get('messages_to_flush'):
            await flush_messages(
                chat_id=message.chat.id,
                message_ids_to_flush=data['messages_to_flush']
            )

    await clear_context(message, state)

    await message.reply(
        text='You in the tracking section. Here you can create a new tracking 📍 that will '
             'notify you of a change of course over a period of time ⏳. '
             'Also you can list and edit your existing trackings',
        reply_markup=tracking_menu_kb()
    )

    await TrackingFSM.menu.set()


@dp.message_handler(Text(equals='New tracking 📍'), state=TrackingFSM.menu)
async def process_currency_to_get_input(message: types.Message, state: FSMContext):
    currencies = get_exchange_currencies(await get_exchange_rate_list())
    currencies_to_get = list(currencies.keys())

    chunks = divide_list_by(currencies_to_get, n=5)

    await message.reply(
        text='To create a new tracking, you need to select 2 currencies 💱 and set a '
             'time interval ⏳ at which you will be notified 📥 of a change in the exchange rate',
        reply_markup=back_to_tracking_menu_kb()
    )
    await asyncio.sleep(0.5)
    choose_message = await message.bot.send_message(
        chat_id=message.chat.id,
        text='Choose the currency you want to <b>GET</b>:',
        reply_markup=pages_inline_kb(
            page_elements=chunks[0],
            next_page_number=1 if len(chunks) > 1 else None
        )
    )

    async with state.proxy() as data:
        data['messages_to_flush'] = [choose_message.message_id]
        data['page_chunks'] = chunks
        data['currencies'] = currencies

    await NewTrackingFSM.currency_to_get_input.set()


@dp.callback_query_handler(state=NewTrackingFSM.currency_to_get_input)
async def process_currency_to_spend_input(
        query: types.CallbackQuery,
        state: FSMContext
):
    async with state.proxy() as data:
        data['to_get'] = query.data
        chunks = divide_list_by(data['currencies'][query.data], n=5)
        data['page_chunks'] = chunks

    await query.message.edit_text(
        text=f'You chose <b>{query.data}</b>. '
             f'Choose the currency you want to <b>SPEND</b> to buy <b>{query.data}</b>:',
        reply_markup=pages_inline_kb(
            chunks[0],
            next_page_number=1
        )
    )

    await NewTrackingFSM.currency_to_spend_input.set()


@dp.callback_query_handler(state=NewTrackingFSM.currency_to_spend_input)
async def process_currency_interval_input(
        query: types.CallbackQuery,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
        client_repo: ClientRepository,
):
    chunks = divide_list_by(TIME_INTERVALS, n=4)
    client = await client_repo.get_by_chat_id(chat_id=query.message.chat.id)
    list_of_trackings = await pair_tracking_repo.get_by_client_id(client_id=client.id)

    async with state.proxy() as data:

        for tracking in list_of_trackings:
            if tracking.currency_to_get == data['to_get'] and tracking.currency_to_spend == query.data:
                await query.message.edit_text(
                    text='<b>You already tracking this currencies. Pick something else.</b>'
                )

                await asyncio.sleep(1.5)
                currencies = get_exchange_currencies(await get_exchange_rate_list())
                currencies_to_get = list(currencies.keys())

                chunks = divide_list_by(currencies_to_get, n=5)
                new_choose_message = await query.bot.send_message(
                    chat_id=query.message.chat.id,
                    text='Choose the currency you want to <b>GET</b>:',
                    reply_markup=pages_inline_kb(
                        page_elements=chunks[0],
                        next_page_number=1 if len(chunks) > 1 else None
                    )
                )

                data['messages_to_flush'].append(new_choose_message.message_id)
                data['page_chunks'] = chunks

                await NewTrackingFSM.currency_to_get_input.set()
                return

        data['to_spend'] = query.data
        data['page_chunks'] = chunks
        data['row_width'] = 4

    await query.message.edit_text(
        text='Select a time interval ⌛ to receive notifications 📥',
        reply_markup=pages_inline_kb(
            page_elements=chunks[0],
            row_width=4,
            next_page_number=1 if len(chunks) > 1 else None
        )
    )

    await NewTrackingFSM.interval_input.set()


@dp.callback_query_handler(state=NewTrackingFSM.interval_input)
async def process_creation_of_new_tracking(
        query: types.CallbackQuery,
        state: FSMContext,
        client_repo: ClientRepository,
        pair_tracking_repo: PairTrackingRepository
):
    async with state.proxy() as data:
        to_get = data['to_get']
        to_spend = data['to_spend']
        interval = query.data
        messages_to_flush = data['messages_to_flush']

    client = await client_repo.get_by_chat_id(chat_id=query.message.chat.id)
    await pair_tracking_repo.create(
        InPairTrackingSchema(
            currency_to_get=to_get,
            currency_to_spend=to_spend,
            interval=interval,
            client_id=client.id,
            last_price=await get_price_from_pair(to_get, to_spend)
        )
    )

    await query.message.bot.send_message(
        chat_id=query.message.chat.id,
        text=f'Your tracking is ready and just had started ✅. '
             f'You can edit your tracking in "List tracking 🗂" section:\n\n'
             f'Currency to get: <b>{to_get}</b>\n'
             f'Currency to spend: <b>{to_spend}</b>\n'
             f'Time interval: <b>{interval}</b>\n'
             f'Pair: <b>{to_get} → {to_spend}</b>',
        reply_markup=tracking_menu_kb()
    )

    await flush_messages(chat_id=query.message.chat.id, message_ids_to_flush=messages_to_flush)
    await state.reset_data()
    await TrackingFSM.menu.set()


@dp.message_handler(Text(equals='List tracking 🗂'), state=TrackingFSM.menu)
async def process_list_of_tracking(
        message: types.Message,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
        client_repo: ClientRepository
):
    client = await client_repo.get_by_chat_id(chat_id=message.chat.id)
    pairs = await pair_tracking_repo.get_by_client_id(client_id=client.id)

    if not pairs:
        await message.reply(
            text='You don\'t have active tracking. Click on <b>New tracking 📍</b> to create one'
        )
        return

    chunks = divide_list_by(list(map(lambda pair: pair.mnemonic_pair.replace('_', '→'), pairs)), n=4)

    await message.reply(
        text='In this section you can manage your treks 🎯. '
             'To go to the management ⚙, select your currency pair under ⬇ the message',
        reply_markup=back_to_tracking_menu_kb()
    )
    mess = await message.bot.send_message(
        chat_id=message.chat.id,
        text='Choose you currency pair down below ⬇',
        reply_markup=pages_inline_kb(
            page_elements=chunks[0],
            next_page_number=1 if len(chunks) > 1 else None
        )
    )

    async with state.proxy() as data:
        data['page_chunks'] = chunks
        data['messages_to_flush'] = [mess.message_id]

    await ListTrackingFSM.tracking_input.set()


@dp.callback_query_handler(state=ListTrackingFSM.tracking_input)
async def process_pair_choose(
        query: types.CallbackQuery,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
        client_repo: ClientRepository
):
    currency_to_get, currency_to_spend = query.data.split('→')

    client = await client_repo.get_by_chat_id(chat_id=query.message.chat.id)
    db_pair = await pair_tracking_repo.get_by_client_id_and_currency(
        client_id=client.id,
        currency_to_get=currency_to_get,
        currency_to_spend=currency_to_spend
    )

    async with state.proxy() as data:
        data['tracking_id'] = db_pair.id

    await query.message.edit_text(
        text=get_tracking_info_text(
            pair=query.data,
            interval=db_pair.interval,
            active=db_pair.is_active,
            last_price=db_pair.last_price,
            current_price=await get_price_from_pair(currency_to_get, currency_to_spend)
        ),
        reply_markup=pair_settings_kb(is_pair_active=db_pair.is_active)
    )

    await ListTrackingFSM.tracking_change.set()


@dp.callback_query_handler(lambda query: query.data == 'toggle_on' or query.data == 'toggle_off',
                           state=ListTrackingFSM.tracking_change)
async def process_toggle_on_tracking(
        query: types.CallbackQuery,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
):
    async with state.proxy() as data:
        tracking_id = data['tracking_id']

    db_pair = await pair_tracking_repo.get_by_id(entry_id=tracking_id)

    if query.data == 'toggle_on':
        await pair_tracking_repo.update_by_id(entry_id=tracking_id, is_active=True)

        await query.message.edit_text(
            text=get_tracking_info_text(
                pair=db_pair.mnemonic_pair.replace('_', '→'),
                interval=db_pair.interval,
                active=True,
                last_price=db_pair.last_price,
                current_price=await get_price_from_pair(db_pair.currency_to_get, db_pair.currency_to_spend)
            ),
            reply_markup=pair_settings_kb(is_pair_active=True)
        )

    if query.data == 'toggle_off':
        await pair_tracking_repo.update_by_id(entry_id=tracking_id, is_active=False)

        await query.message.edit_text(
            text=get_tracking_info_text(
                pair=db_pair.mnemonic_pair.replace('_', '→'),
                interval=db_pair.interval,
                active=False,
                last_price=db_pair.last_price,
                current_price=await get_price_from_pair(db_pair.currency_to_get, db_pair.currency_to_spend)
            ),
            reply_markup=pair_settings_kb(is_pair_active=False)
        )


@dp.callback_query_handler(lambda query: query.data == 'back_to_pairs',
                           state=ListTrackingFSM.tracking_change)
async def process_pair_choose_from_back_button(
        query: types.CallbackQuery,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
        client_repo: ClientRepository
):
    client = await client_repo.get_by_chat_id(chat_id=query.message.chat.id)
    pairs = await pair_tracking_repo.get_by_client_id(client_id=client.id)

    if not pairs:
        await query.message.edit_text(
            text='You no longer have the pairs you are tracking 🎯. Click the button ⬇ to return to the tracking menu'
        )
        return

    chunks = divide_list_by(list(map(lambda pair: pair.mnemonic_pair.replace('_', '→'), pairs)), n=4)
    await query.message.edit_text(
        text='Choose you currency pair down below ⬇',
        reply_markup=pages_inline_kb(
            page_elements=chunks[0],
            next_page_number=1 if len(chunks) > 1 else None
        )
    )

    async with state.proxy() as data:
        data['page_chunks'] = chunks

    await ListTrackingFSM.tracking_input.set()


@dp.callback_query_handler(lambda query: query.data == 'delete',
                           state=ListTrackingFSM.tracking_change)
async def process_pair_tracking_deletion(
        query: types.CallbackQuery,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
        client_repo: ClientRepository
):
    async with state.proxy() as data:
        db_pair = await pair_tracking_repo.get_by_id(entry_id=data['tracking_id'])
        await pair_tracking_repo.delete_by_id(entry_id=data['tracking_id'])

        mnemonic_pair_name = db_pair.mnemonic_pair

    await query.answer(f'{mnemonic_pair_name.replace("_", "→")} tracking has been DELETED',
                       show_alert=True)

    await process_pair_choose_from_back_button(query, state, pair_tracking_repo, client_repo)


@dp.callback_query_handler(lambda query: query.data == 'change_interval',
                           state=ListTrackingFSM.tracking_change)
async def process_interval_change_input(
        query: types.CallbackQuery,
        state: FSMContext
):
    chunks = divide_list_by(TIME_INTERVALS, n=4)

    async with state.proxy() as data:
        data['page_chunks'] = chunks
        data['row_width'] = 4

    await query.message.edit_text(
        text='Choose a new interval ⌛ to proceed:',
        reply_markup=pages_inline_kb(
            page_elements=chunks[0],
            row_width=4,
            next_page_number=1 if len(chunks) > 1 else None
        )
    )

    await ListTrackingFSM.interval_input_change.set()


@dp.callback_query_handler(state=ListTrackingFSM.interval_input_change)
async def process_interval_change(
        query: types.CallbackQuery,
        state: FSMContext,
        pair_tracking_repo: PairTrackingRepository,
):
    async with state.proxy() as data:
        db_pair = await pair_tracking_repo.get_by_id(entry_id=data['tracking_id'])
        await pair_tracking_repo.update_by_id(entry_id=data['tracking_id'], interval=query.data)

    await query.answer('Tracking interval successfully has been changed')

    await query.message.edit_text(
        text=get_tracking_info_text(
            pair=db_pair.mnemonic_pair.replace('_', '→'),
            interval=query.data,
            active=db_pair.is_active,
            last_price=db_pair.last_price,
            current_price=await get_price_from_pair(db_pair.currency_to_get, db_pair.currency_to_spend)
        ),
        reply_markup=pair_settings_kb(is_pair_active=db_pair.is_active)
    )

    await ListTrackingFSM.tracking_change.set()
