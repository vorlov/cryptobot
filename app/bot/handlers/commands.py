import logging

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text

from app.db.repositories.user import ClientRepository
from app.db.tables.user import UserLocales
from app.models.schema.user import InClientSchema, InBotUserSchema

from app.bot.keyboards import main_menu_kb
from app.bot.states import MenuFSM

from app.bot import dp, bot
from app.bot.handlers.utils import flush_messages, clear_context

log = logging.getLogger('bot')


@dp.message_handler(commands=['start'], state='*')
async def process_start_command(
        message: types.Message,
        state: FSMContext,
        client_repo: ClientRepository
):
    await clear_context(message, state)

    await message.bot.send_sticker(chat_id=message.chat.id,
                                   sticker='CAACAgIAAxkBAAEUUO9ijMWINr9bfz2HL2zJ5tui4qKzeAACbwAD29t-AAGZW1Coe5OAdCQE')
    await message.reply(
        text='Hi there! You in the main menu. '
             'This is a bot that will help you <b>effectively and quickly</b> monitor currency exchange',
        reply_markup=main_menu_kb(),
        parse_mode='HTML'
    )

    client = await message.bot.get_chat_member(chat_id=message.chat.id, user_id=message.from_user.id)

    db_client = await client_repo.get_by_chat_id_or_none(chat_id=message.chat.id)

    if not db_client:
        user_locale = UserLocales.en if str(client.user.locale) != 'uk' else UserLocales.uk
        db_client = await client_repo.create(InClientSchema(
            locale=user_locale,
            is_active=True
        ))

        await client_repo.bot_repository.create(InBotUserSchema(
            chat_id=message.from_user.id,
            username=message.from_user.username,
            first_name=message.from_user.first_name,
            last_name=message.from_user.last_name,
            client_id=db_client.id
        ))

    await MenuFSM.main_menu.set()


@dp.message_handler(Text(equals='Main menu 🔙'), state='*')
async def process_main_menu(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if data.get('messages_to_flush'):
            await flush_messages(
                chat_id=message.chat.id,
                message_ids_to_flush=data['messages_to_flush']
            )

    await clear_context(message, state)

    await message.reply(
        text='Hi there! This is a bot that will help you <b>effectively and quickly</b> monitor currency exchange',
        reply_markup=main_menu_kb(),
        parse_mode='HTML'
    )
    await MenuFSM.main_menu.set()
