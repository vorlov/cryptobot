import logging
from typing import List, Dict

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.filters import OrFilter

from app.bot import bot, dp
from app.bot.keyboards import navigate_currencies_cb, pages_inline_kb
from app.bot.states import NewTrackingFSM, ExchangeFSM, ListTrackingFSM

from aiogram.utils.exceptions import MessageError

TIME_INTERVALS = [
    '1m', '5m', '10m', '30m',
    '1h', '4h', '6h', '12h',
    '1d', '2d', '1w', '2w',
    '3w', '4w'
]

log = logging.getLogger('bot')


def get_tracking_info_text(
        pair: str,
        interval: str,
        last_price: float,
        current_price: float,
        active: bool
):
    emoji = "📈" if current_price > last_price else "📉"
    if current_price == last_price:
        emoji = ''

    text = f'Here is your tracking 🎯. You can enable 🟢 or disable 🔴 tracking, ' \
           f'change time interval ⏳ by clicking buttons under the message:\n\n' \
           f'Tracking pair: <b>{pair}</b>\n' \
           f'Time interval: <b>{interval}</b>\n' \
           f'Last price: <b>{last_price}</b>\n' \
           f'Current price: <b>{current_price}</b>{emoji}\n' \
           f'Active: {"🟢" if active else "🔴"}'

    return text


# To produce this handler you need to fill a few kwargs in data:
# page_chunks: list if list of context you want to show
# row_width: row width of inline keyboard
# Also you need to add state in states
@dp.callback_query_handler(
    OrFilter(
        navigate_currencies_cb.filter(action='back'),
        navigate_currencies_cb.filter(action='forward')
    ),
    state=(NewTrackingFSM.currency_to_get_input,
           NewTrackingFSM.currency_to_spend_input,
           NewTrackingFSM.interval_input,
           ExchangeFSM.currency_to_get_input,
           ExchangeFSM.currency_to_spend_input,
           ListTrackingFSM.tracking_input,
           ListTrackingFSM.interval_input_change)
)
async def process_pages(
        query: types.CallbackQuery,
        callback_data: Dict[str, str],
        state: FSMContext
):
    async with state.proxy() as data:
        pages = data['page_chunks']
        if data.get('row_width'):
            row_width = data['row_width']
        else:
            row_width = None

        next_page = int(callback_data['to_page'])

    await query.message.edit_reply_markup(
        reply_markup=pages_inline_kb(
            pages[next_page],
            row_width=row_width or 2,
            next_page_number=(next_page + 1) if next_page + 1 < len(pages) else None,
            previous_page_number=(next_page - 1) if next_page - 1 >= 0 else None
        )
    )


def divide_list_by(to_divide: List, n: int):
    return [to_divide[i:i + n] for i in range(0, len(to_divide), n)]


def list_union(list1: List, list2: List):
    return list1 + list(filter(lambda currency: currency not in list1, list2))


def perform_union_with_ordering(list1: List, list2: List):
    for l1 in list1:
        if l1 in list2:
            list2.remove(l1)
        else:
            list1.remove(l1)


async def clear_context(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        message_id_to_delete = data.get('callback_query_message_id')

    if message_id_to_delete:
        try:
            await message.bot.delete_message(chat_id=message.chat.id, message_id=message_id_to_delete)
        except Exception as e:
            log.exception(e)
    await state.finish()


async def flush_messages(chat_id: int, message_ids_to_flush: List[int]):
    for message_id in message_ids_to_flush:
        try:
            await bot.delete_message(chat_id=chat_id, message_id=message_id)
        except MessageError:
            log.warning(f'flush_messages<chat_id:{chat_id}> failed to delete message with id {message_id}')
