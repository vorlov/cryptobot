from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware

from app.db.repositories.user import ClientRepository
from app.db.repositories.action import ExchangeActionRepository
from app.db.repositories.tracking import PairTrackingRepository
from app.db.session import async_session

from aiogram.contrib.middlewares.logging import LoggingMiddleware


def inject_async_session_to_data(data: dict):
    session = async_session()

    data['asession'] = session
    data['client_repo'] = ClientRepository(session)
    data['exchange_action_repo'] = ExchangeActionRepository(session)
    data['pair_tracking_repo'] = PairTrackingRepository(session)


class RepoInjector(BaseMiddleware):
    async def on_pre_process_message(self, message: types.Message, data: dict):
        inject_async_session_to_data(data)

    async def on_post_process_message(self, message: types.Message, results, data: dict):
        await data['asession'].close()

    async def on_pre_process_callback_query(self, query: types.CallbackQuery, data: dict):
        inject_async_session_to_data(data)

    async def on_post_process_callback_query(self, query: types.CallbackQuery, results, data: dict):
        await data['asession'].close()
