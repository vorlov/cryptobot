from aiogram import Dispatcher
from app.bot.middlewares.repo import RepoInjector


def setup(dp: Dispatcher):
    dp.middleware.setup(RepoInjector())
