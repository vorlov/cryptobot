from aiogram.dispatcher.filters.state import State, StatesGroup


class MenuFSM(StatesGroup):
    main_menu = State()


class ExchangeFSM(StatesGroup):
    currency_to_spend_input = State()
    currency_to_get_input = State()


class TrackingFSM(StatesGroup):
    menu = State()

    # Add new tracking


class NewTrackingFSM(StatesGroup):
    currency_to_spend_input = State()
    currency_to_get_input = State()
    interval_input = State()


class ListTrackingFSM(StatesGroup):
    tracking_input = State()

    tracking_change = State()
    interval_input_change = State()
