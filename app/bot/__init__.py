from aiogram import Bot, Dispatcher, types

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.fsm_storage.redis import RedisStorage2

from app.core.config import settings

bot = Bot(token=settings.bot.token, parse_mode=types.ParseMode.HTML)
_redis_conf = settings.bot.storage.redis_dsn

if settings.bot.storage.mode == 'redis':
    storage = RedisStorage2(
        password=_redis_conf.password,
        host=_redis_conf.host,
        port=_redis_conf.port,
        db=int(_redis_conf.path[1:]),
    )
elif settings.bot.storage.mode == 'memory':
    storage = MemoryStorage()
else:
    raise TypeError('Wrong bot storage mode. Possible modes: redis, memory')

dp = Dispatcher(bot, storage=storage)


def setup_aiogram():
    from app.bot import middlewares

    middlewares.setup(dp)

    import app.bot.handlers
