from typing import Dict, Optional
from app.api.coinpay import get_exchange_rate_list


def get_exchange_currencies(data: Dict) -> Dict:
    final_data = {}

    rates = data['rates']

    for i in range(len(rates)):
        c_to_spend, c_to_get = rates[i]['pair'].split('_')

        if final_data.get(c_to_get) is None:
            final_data[c_to_get] = [c_to_spend]
        else:
            final_data[c_to_get].append(c_to_spend)
    return final_data


async def get_price_from_pair(
        currency_to_get: str,
        currency_to_spend: str,
) -> Optional[float]:
    data = await get_exchange_rate_list()
    rates = data['rates']

    for pair_data in rates:
        if pair_data['pair'] == f'{currency_to_get}_{currency_to_spend}':
            return pair_data['base_currency_price']
