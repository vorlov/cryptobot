import logging
from typing import Dict, Optional

import aiohttp

from app.api.error import StatusCodeIsNotCorrect, WrongResponseData

log = logging.getLogger('api')
COINPAY_BASE_URL = 'https://coinpay.org.ua/'


async def process_response(message_prefix: str, response: aiohttp.ClientResponse) -> Dict:
    if response.status not in range(200, 300):
        raise StatusCodeIsNotCorrect(message_prefix + f' status code is {response.status}')

    data = await response.json(content_type='application/json')
    if data.get('status') == 'error':
        raise WrongResponseData(message_prefix + f' api returned error: "{data.get("error")}"')

    elif data.get('status') == 'success':
        log.info(message_prefix + f' got success with status code {response.status}')
        return data

    else:
        raise WrongResponseData(message_prefix + f' api returned unknown data: {data}')


async def get_list_of_active_pairs() -> Dict:
    async with aiohttp.request('GET', COINPAY_BASE_URL + '/api/v1/pair') as response:
        return await process_response('get_list_of_active_pairs<>', response)


async def get_exchange_rate_list() -> Dict:
    async with aiohttp.request('GET', COINPAY_BASE_URL + '/api/v1/exchange_rate') as response:
        return await process_response('get_exchange_rate_list<>', response)
