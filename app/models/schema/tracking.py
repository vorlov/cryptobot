from datetime import datetime
from typing import Optional, Literal

from pydantic import validator, fields, Field
from app.models.schema.base import BaseSchema


class PairTrackingBaseSchema(BaseSchema):
    currency_to_spend: str
    currency_to_get: str
    last_price: float
    interval: Literal[
        '1m', '5m', '10m', '30m',
        '1h', '4h', '6h', '12h',
        '1d', '2d', '1w', '2w',
        '3w', '4w'
    ]

    client_id: int


class InPairTrackingSchema(PairTrackingBaseSchema):
    last_notified: Optional[datetime] = Field(default_factory=datetime.now)
    is_active: Optional[bool] = True
    mnemonic_pair: Optional[str]

    @validator('currency_to_spend', 'currency_to_get')
    def check_length(cls, v: str, field: fields.ModelField):
        if len(v) > 5:
            raise ValueError(f'{field.name} can not be greater than 5')
        return v

    @validator('mnemonic_pair', pre=True, always=True)
    def fill_mnemonic_pair(cls, v, values):

        if v is None:
            return f"{values['currency_to_get']}_{values['currency_to_spend']}"
        return v


class PairTrackingSchema(PairTrackingBaseSchema):
    id: int
    created_at: datetime
    updated_at: datetime
    last_notified: datetime
    is_active: bool
    mnemonic_pair: str
