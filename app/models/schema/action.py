from datetime import datetime
from pydantic import validator, fields
from app.models.schema.base import BaseSchema


class ExchangeActionBaseSchema(BaseSchema):
    currency_to_spend: str
    currency_to_get: str

    client_id: int


class InExchangeActionSchema(ExchangeActionBaseSchema):
    @validator('currency_to_spend', 'currency_to_get')
    def check_length(cls, v: str, field: fields.ModelField):
        if len(v) > 5:
            raise ValueError(f'{field.name} can not be greater than 5')
        return v


class ExchangeActionSchema(ExchangeActionBaseSchema):
    id: int
    created_at: datetime
    updated_at: datetime
