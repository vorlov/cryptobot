from enum import Enum
from typing import Optional
from datetime import datetime
from pydantic import validator, root_validator

from app.models.schema.base import BaseSchema
from app.db.tables.user import UserLocales


class BotUserBaseSchema(BaseSchema):
    chat_id: int
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]

    client_id: int


class InBotUserSchema(BotUserBaseSchema):
    @root_validator
    def check_bot_user_fields(cls, values):
        if not any([
            values['username'],
            values['first_name'],
            values['last_name']
        ]):
            raise ValueError('at least one field must be filled among username, first_name or last_name')
        return values


class BotUserSchema(BotUserBaseSchema):
    id: int
    created_at: datetime
    updated_at: datetime


class InClientSchema(BaseSchema):
    locale: Optional[UserLocales]
    is_active: Optional[bool]


class ClientSchema(BaseSchema):
    id: int
    created_at: datetime
    updated_at: datetime
    locale: UserLocales
    is_active: bool
    bot: Optional[BotUserSchema]
