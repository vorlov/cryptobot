from datetime import datetime

from sqlalchemy import Column, VARCHAR, ForeignKey, Integer, DateTime, Boolean, Float

from app.db.base_class import Base
from app.db.tables.mixins import TimeMixin


class PairTracking(Base, TimeMixin):
    __tablename__ = 'pair_tracking'

    currency_to_spend: str = Column(VARCHAR(5), nullable=False)
    currency_to_get: str = Column(VARCHAR(5), nullable=False)
    interval: str = Column(VARCHAR(5), nullable=False)
    last_notified: datetime = Column(DateTime, nullable=False, default=datetime.now)
    last_price: float = Column(Float)
    is_active: bool = Column(Boolean, nullable=False, default=True)
    mnemonic_pair: str = Column(VARCHAR(10), nullable=False)

    client_id: int = Column(Integer, ForeignKey('client.id', ondelete='CASCADE'), nullable=False)
