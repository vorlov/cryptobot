import enum
from typing import Optional

from sqlalchemy import Column, Integer, Text, Boolean, Enum, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base
from app.db.tables.mixins import TimeMixin


class UserLocales(enum.Enum):
    en = 'en'
    uk = 'uk'


class BotClient(Base, TimeMixin):
    chat_id: int = Column(Integer, nullable=False, unique=True)
    username: Optional[str] = Column(Text)

    first_name: Optional[str] = Column(Text)
    last_name: Optional[str] = Column(Text)

    client_id: int = Column(Integer, ForeignKey('client.id', ondelete='CASCADE'), nullable=False, unique=True)


class Client(Base, TimeMixin):
    is_active: bool = Column(Boolean, nullable=False, default=True)
    locale: str = Column(Enum(UserLocales), default=UserLocales.en)

    bot = relationship('BotClient', lazy='subquery', uselist=False)
