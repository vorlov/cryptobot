from sqlalchemy import Column, VARCHAR, ForeignKey, Integer

from app.db.base_class import Base
from app.db.tables.mixins import TimeMixin


class ExchangeAction(Base, TimeMixin):
    __tablename__ = 'currency_action'

    currency_to_spend: str = Column(VARCHAR(5), nullable=False)
    currency_to_get: str = Column(VARCHAR(5), nullable=False)

    client_id: int = Column(Integer, ForeignKey('client.id', ondelete='CASCADE'), nullable=False)
