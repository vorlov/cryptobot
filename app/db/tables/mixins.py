from datetime import datetime
from sqlalchemy import Column, DateTime


class TimeMixin:
    created_at: datetime = Column(DateTime, default=datetime.now)
    updated_at: datetime = Column(DateTime, default=datetime.now, onupdate=datetime.now)
