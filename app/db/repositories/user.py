import logging
from typing import Type

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select

from app.db.repositories.base import BaseRepository, IN_SCHEMA, SCHEMA, TABLE
from app.models.schema.user import (BotUserSchema, InBotUserSchema,
                                    ClientSchema, InClientSchema)
from app.db.tables.user import Client, BotClient
from app.db.errors import DoesNotExist

log = logging.getLogger('db')


class BotClientRepository(BaseRepository[InBotUserSchema, BotUserSchema, BotClient]):
    @property
    def _table(self) -> Type[BotClient]:
        return BotClient

    @property
    def _schema(self) -> Type[BotUserSchema]:
        return BotUserSchema

    @property
    def _in_schema(self) -> Type[InBotUserSchema]:
        return InBotUserSchema


class ClientRepository(BaseRepository[InClientSchema, ClientSchema, Client]):
    def __init__(self, db_session: AsyncSession, *args, **kwargs):
        self.bot_repository: BotClientRepository = BotClientRepository(db_session)

        super().__init__(db_session, *args, **kwargs)

    @property
    def _table(self) -> Type[Client]:
        return Client

    @property
    def _schema(self) -> Type[ClientSchema]:
        return ClientSchema

    @property
    def _in_schema(self) -> Type[InClientSchema]:
        return InClientSchema

    async def get_by_chat_id(self, chat_id: int) -> ClientSchema:
        query = select(self._table). \
            join(self._table.bot). \
            filter(BotClient.chat_id == chat_id)

        if entry := await self.get_one_from_query(query):
            log.debug(f'{self._table.__name__}>{BotClient.__name__}<chat_id:{chat_id}> found')
            return entry

        raise DoesNotExist(f'{self._table.__name__}>{BotClient.__name__}<chat_id:{chat_id}> not found')

    async def get_by_chat_id_or_none(self, chat_id: int) -> ClientSchema:
        query = select(self._table). \
            join(self._table.bot). \
            filter(BotClient.chat_id == chat_id)

        if entry := await self.get_one_from_query(query):
            log.debug(f'{self._table.__name__}>{BotClient.__name__}<chat_id:{chat_id}> found')
            return entry

        log.debug(f'{self._table.__name__}>{BotClient.__name__}<chat_id:{chat_id}> not found. Returning None')
