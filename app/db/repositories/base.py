import abc
import logging
from typing import Generic, TypeVar, Type, List, Optional

from sqlalchemy import select, insert, func, update, delete
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.errors import DoesNotExist
from app.models.schema.base import BaseSchema

IN_SCHEMA = TypeVar("IN_SCHEMA", bound=BaseSchema)
SCHEMA = TypeVar("SCHEMA", bound=BaseSchema)
TABLE = TypeVar("TABLE")

log = logging.getLogger('db')


class BaseRepository(Generic[IN_SCHEMA, SCHEMA, TABLE], metaclass=abc.ABCMeta):
    def __init__(self, db_session: AsyncSession, *args, **kwargs) -> None:
        self._db_session: AsyncSession = db_session

    @property
    @abc.abstractmethod
    def _table(self) -> Type[TABLE]:
        ...

    @property
    @abc.abstractmethod
    def _schema(self) -> Type[SCHEMA]:
        ...

    @property
    @abc.abstractmethod
    def _in_schema(self) -> Type[IN_SCHEMA]:
        ...

    async def get_one_from_query(self, query: str) -> Optional[SCHEMA]:
        qresult = await self._db_session.execute(query)
        entry = qresult.scalar_one_or_none()

        if entry:
            return self._schema.from_orm(entry)

    async def get_many_from_query(self, query: str) -> List[SCHEMA]:
        result = await self._db_session.execute(query)
        return list(map(self._schema.from_orm, result.scalars()))

    async def get_by_id(self, entry_id: int) -> SCHEMA:
        query = select(self._table).where(self._table.id == entry_id)

        if entry := await self.get_one_from_query(query):
            log.debug(f'{self._table.__name__}<id:{entry_id}> found')
            return entry

        raise DoesNotExist(f"{self._table.__name__}<id:{entry_id}> does not exist")

    async def get_by_id_or_none(self, entry_id: int) -> SCHEMA:
        query = select(self._table).where(self._table.id == entry_id)

        if entry := await self.get_one_from_query(query):
            log.debug(f'{self._table.__name__}<id:{entry_id}> found')
            return entry

        log.debug(f'{self._table.__name__}<id:{entry_id}> not found. Returning None')

    async def get_all(self) -> List[SCHEMA]:
        query = select(self._table)

        if entries := await self.get_many_from_query(query):
            log.debug(f'{self._table.__name__}<> found {len(entries)} rows')
            return entries

        raise DoesNotExist(f'{self._table.__name__} no rows found')

    async def get_records_count(self) -> int:
        query = select(func.count()).select_from(self._table)
        result = await self.get_one_from_query(query)

        log.debug(f'{self._table.__name__}<> found {result} rows')
        return result

    async def create(self, in_schema: IN_SCHEMA) -> SCHEMA:
        query = insert(self._table).values(**in_schema.dict())
        result = await self._db_session.execute(query)
        await self._db_session.commit()

        log.debug(f'{self._table.__name__}<id:{result.inserted_primary_key[0]}> created')
        return await self.get_by_id(result.inserted_primary_key[0])

    async def update_by_id(self, entry_id: int, **values):
        if not getattr(self._table, 'id', None):
            raise AttributeError(f'{self._table.__name__}<id:{entry_id}> has no `id` column')

        query = update(self._table).where(self._table.id == entry_id).values(**values)

        await self._db_session.execute(query)
        await self._db_session.commit()

        log.debug(f'{self._table.__name__}<id:{entry_id}> updated with values {values}')

    async def delete_by_id(self, entry_id: int):
        if not getattr(self._table, 'id', None):
            raise AttributeError(f'{self._table.__name__}<id:{entry_id}> has no `id` column')

        query = delete(self._table).where(self._table.id == entry_id)

        await self._db_session.execute(query)
        await self._db_session.commit()

        log.debug(f'{self._table.__name__}<id:entry_id> deleted')
