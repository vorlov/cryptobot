import logging
from typing import Type, List

from sqlalchemy import select

from app.db.repositories.base import BaseRepository
from app.db.tables.action import ExchangeAction
from app.db.errors import DoesNotExist

from app.models.schema.action import ExchangeActionSchema, InExchangeActionSchema

log = logging.getLogger('db')


class ExchangeActionRepository(BaseRepository[InExchangeActionSchema, ExchangeActionSchema, ExchangeAction]):
    @property
    def _table(self) -> Type[ExchangeAction]:
        return ExchangeAction

    @property
    def _schema(self) -> Type[ExchangeActionSchema]:
        return ExchangeActionSchema

    @property
    def _in_schema(self) -> Type[InExchangeActionSchema]:
        return InExchangeActionSchema

    async def get_latest_currency_to_get_by_user_id(self, user_id: int) -> List[ExchangeActionSchema]:
        query = select(self._table). \
            distinct(self._table.currency_to_get). \
            where(self._table.client_id == user_id)

        if entries := await self.get_many_from_query(query):
            log.debug(f'{self._table.__name__}<user_id:{user_id}> found {len(entries)} rows')
            entries = sorted(entries, key=lambda entry: entry.created_at, reverse=True)
            return entries

        log.debug(f'{self._table.__name__}<user_id:{user_id}> not found')
        return []

    async def get_latest_currency_to_spend_by_user_id(self, user_id: int) -> List[ExchangeActionSchema]:
        query = select(self._table). \
            distinct(self._table.currency_to_spend). \
            where(self._table.client_id == user_id)

        if entries := await self.get_many_from_query(query):
            log.debug(f'{self._table.__name__}<user_id:{user_id}> found {len(entries)} rows')
            entries = sorted(entries, key=lambda entry: entry.created_at, reverse=True)

            return entries

        log.debug(f'{self._table.__name__}<user_id:{user_id}> not found')
        return []
