import logging
from typing import Type, List, Optional

from sqlalchemy import select, and_

from app.db.errors import DoesNotExist
from app.db.repositories.base import BaseRepository, IN_SCHEMA, SCHEMA, TABLE
from app.models.schema.tracking import PairTrackingSchema, InPairTrackingSchema
from app.db.tables.tracking import PairTracking

log = logging.getLogger('db')


class PairTrackingRepository(BaseRepository[InPairTrackingSchema, PairTrackingSchema, PairTracking]):
    @property
    def _table(self) -> Type[PairTracking]:
        return PairTracking

    @property
    def _schema(self) -> Type[PairTrackingSchema]:
        return PairTrackingSchema

    @property
    def _in_schema(self) -> Type[InPairTrackingSchema]:
        return InPairTrackingSchema

    async def get_by_client_id(self, client_id: int) -> Optional[List[PairTrackingSchema]]:
        query = select(self._table).where(self._table.client_id == client_id)

        entries = await self.get_many_from_query(query)
        log.debug(f'{self._table.__name__}<client_id:{client_id}> found {len(entries)} rows')
        return entries

    async def get_by_client_id_and_currency(
            self,
            client_id: int,
            currency_to_get: str,
            currency_to_spend: str
    ) -> PairTrackingSchema:
        query = select(self._table).where(
            and_(
                self._table.client_id == client_id,
                self._table.currency_to_get == currency_to_get,
                self._table.currency_to_spend == currency_to_spend
            )
        )

        if entry := await self.get_one_from_query(query):
            log.debug(f'{self._table.__name__}<client_id:{client_id}, {currency_to_get}>{currency_to_spend}>'
                      f'found')
            return entry
        raise DoesNotExist(f'{self._table.__name__}<client_id:{client_id}, {currency_to_get}>{currency_to_spend}>'
                           f'not found')

    async def get_active_trackings(self) -> Optional[List[PairTrackingSchema]]:
        query = select(self._table).where(self._table.is_active.is_(True))

        if entries := await self.get_many_from_query(query):
            log.debug(f'{self._table.__name__}<is_active:true> found {len(entries)} active trackings')
            return entries
        log.warning(f'{self._table.__name__}<is_active:true> not found')