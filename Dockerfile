FROM python:3.9

RUN pip install poetry \
    && apt-get update

WORKDIR /app
ADD poetry.lock pyproject.toml /app/
ADD . /app

RUN poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi \
    && chmod +x wait-for-it.sh \
    && chmod a+x entrypoints/bot.sh \
    && chmod a+x entrypoints/celery.sh \
    && chmod a+x entrypoints/celerybeat.sh