from aiogram.utils import executor
from app.bot import dp, setup_aiogram, bot
from app.core.log.loggers import setup_loggers
from app.core.config import settings


async def on_startup(dp):
    await bot.set_webhook(settings.bot.webhook.host + settings.bot.webhook.path)


if __name__ == '__main__':
    setup_aiogram()
    setup_loggers()

    if settings.bot.mode == 'webhook':
        executor.start_webhook(
            dispatcher=dp,
            webhook_path=settings.bot.webhook.path,
            on_startup=on_startup,
            skip_updates=True,
            host=settings.bot.webhook.server.host,
            port=settings.bot.webhook.server.port
        )

    if settings.bot.mode == 'polling':
        executor.start_polling(
            dispatcher=dp,
            skip_updates=True,
        )
